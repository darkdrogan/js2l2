const xhr = new XMLHttpRequest();
let bigImageDiv;

window.onload = () => {
  bigImageDiv = document.createElement('div');
  bigImageDiv.id = 'bigImage';
  document.body.appendChild(bigImageDiv);
};

function success() {
  const arrayGallery = JSON.parse(xhr.responseText);


  function updateBigImage(source) {
    const image = document.createElement('img');
    image.src = source;
    bigImageDiv.innerHTML = '';
    bigImageDiv.appendChild(image);
  }

  for (const i in arrayGallery) {
    const div = document.createElement('img');
    div.id = arrayGallery[i].name;
    const miniaturesImage = document.createElement('img');
    miniaturesImage.src = arrayGallery[i].miniature;
    miniaturesImage.alt = arrayGallery[i].description;
    miniaturesImage.onclick = () => {
      updateBigImage(arrayGallery[i].origin);
    };
    div.appendChild(miniaturesImage);
    document.body.appendChild(div);
  }
}


function error() {
  const noImage = document.createElement('p');
  noImage.innerText = 'fail. pleace try again later';
  bigImageDiv.innerText = '';
  bigImageDiv.appendChild(noImage);
}

xhr.onreadystatechange = () => {
  if (xhr.readyState === XMLHttpRequest.DONE) {
    if (xhr.status === 200) {
      success();
    } else if (xhr.status >= 400 && xhr.status < 527) {
      error();
    }
  }
};

xhr.open('GET', 'gallery.json');

xhr.timeout = 10000;

xhr.ontimeout = () => {
  console.log('fail');
};

xhr.send(null);
