const xhr = new XMLHttpRequest();

xhr.onreadystatechange = () => {
  if (xhr.readyState == XMLHttpRequest.DONE) {
    if (xhr.status === 200) {
      const div = document.createElement('div');
      div.innerHTML = `up up ${xhr.responseText}`;
      div.className = 'got_it';
      document.body.appendChild(div);
      console.log(xhr.responseURL);
    } else if (xhr.status === 404) {
      const div = document.createElement('div');
      div.innerText = 'page not found \n Error 404!';
      div.className = 'fail';
      document.body.appendChild(div);
    }
  }
};

for (let i = 0; i < 10; i++) {

}

xhr.open('GET', 'index.html');

xhr.timeout = 15000;

xhr.ontimeout = () => {
  console.log('time is out');
};

xhr.send(null);
